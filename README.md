# Passport login backend

## Generated info
### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.

----------------

## Description

This project is a full-stack web application built using Express.js, Node.js, React.js, and TypeScript. It incorporates social login functionality with authentication using Passport.js, supporting login with Google, Microsoft, and other social login providers. Upon successful authentication, the application displays the user's email address and specific details obtained from the social login.


## Technologies Used
- Express.js
- TypeScript
- MongoDB
- Passport.js (with passport-google-oauth20)
- JWT for token-based authentication
- Other middleware such as body-parser, cookie-parser, cors, csurf, express-rate-limit, express-session, helmet, morgan

## Prerequisites
- Node.js (version specified in package.json)
- Docker

## Installation
1. Clone the repository.
2. Navigate to the project directory.
3. Install dependencies:

```
npm install
```

## Docker Setup
The project includes a Docker configuration for running a MongoDB database instance.

1. Ensure Docker is installed on your machine.
2. Navigate to the project directory.
3. Run the following command to spin up the MongoDB Docker container:

```
docker-compose up -d
```

or alternatively running the created script

```
npm run db:dev:up
```

## Start the development server:
```
npm run dev
```

##  Usage
Register your application with the desired social login providers (Google, Facebook, Microsoft).

* [Facebook Developers](https://developers.facebook.com/)
* [Microsoft Azure](https://portal.azure.com/#home/)
* [Google Cloud](https://console.cloud.google.com/)

Configure the necessary environment variables (e.g., client ID, client secret) for the social login providers in a .env file.
Example .env.sample file is contained inside the application resources

Run the application!

## Testing the application
Most vital flows of the application have been covered.
They are covering the controllers, handlers and services, but there is always room for more upgrades and better coverage!

Command for running the tests:
```
npm run test
```

## Features
Full-stack web application with Express.js backend and React.js frontend
Social login functionality supporting Google, Microsoft, and other providers
Secure authentication with Passport.js and JWT tokens
Display user email address and details obtained from social login

## Potential new features
- Add storing for logged in users to see what provider they are using.
- Make a db corellation between users that are logging with different providers but are essentially same user.
    - Potential upgrade in DB to corellate providers and make sure once new one is used to gather them in one user.
    - This could potentially transition it to MySQL instead of Mongo, since relations are introduced.
- Editing of user profiles that are stored in local DB, the cloud porivers values would not be updated.

## Contact
If you have any questions or suggestions, feel free to contact me:
[Email](mailto:aleksandar.radosevic@nqode.io)