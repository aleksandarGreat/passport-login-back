import { connect } from 'mongoose';
import { DbConfig, configuration } from '../configs/configuration';

const dbConfig: DbConfig = configuration.db;
const connectDB = async () => {
  try {
    await connect(dbConfig.uri!);
    console.log('MongoDB connected successfully on the given URL.');
  } catch (err) {
    console.error((err as Error).message);
    // Exit process with failure
    process.exit(1);
  }
};

export default connectDB;