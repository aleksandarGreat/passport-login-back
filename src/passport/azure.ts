import passport from 'passport';
import { Strategy as CurityStrategy, discoverAndCreateClient } from 'passport-curity';
import { AzureConfig, configuration } from '../configs/configuration';
import User from '../models/user.model';

const azureConfig: AzureConfig = configuration.azure;

export async function createCurityClient() {
  return discoverAndCreateClient({
    issuerUrl: `${azureConfig.azureDefaultUrl}/${azureConfig.azureTenantId}/${azureConfig.azureTargetingVersion}`,
    clientID: azureConfig.azureClientId!,
    clientSecret: azureConfig.azureClientSecret,
    redirectUris: [azureConfig.azureCallbackUrl!],
  });
}

createCurityClient().then(client => {
  passport.use(new CurityStrategy({
    client,
    params: {
      scope: 'openid email profile',
    },
  }, async (accessToken, refreshToken, profile, done) => {
    let user = await User.findOne({ providerId: profile.id });
    if (!user) {
      user = new User({
        provider: 'Microsfort/Azure',
        name: profile.name,
        providerId: profile.oid,
        email: profile.email,
      });
      await user.save();
    }
    done(null, user);
  }));
});