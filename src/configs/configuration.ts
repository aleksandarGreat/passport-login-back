import * as dotenv from 'dotenv';
import { join } from 'path';
import * as process from 'process';

dotenv.config({ path: join(process.cwd(), '.env') });

export const configuration = {
    app: {
      port: process.env.PORT,
      corsOrigin: process.env.CORS_ORIGIN,
      successfullLoginUrl: process.env.SUCCESSFULL_LOGIN_URL,
      failedLoginUrl: process.env.FAILED_LOGIN_URL,
    },
    secret: {
      jwtSecret: process.env.JWT_SECRET,
      sessionSecret: process.env.SESSION,
    },
    db: {
      uri: process.env.DB_URL,
    },
    google: {
      googleClientId: process.env.GOOGLE_CLIENT_ID,
      googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
      googleCallbackUrl: process.env.GOOGLE_CALLBACK_URL,
    },
    facebook: {
      facebookClientId: process.env.FACEBOOK_CLIENT_ID,
      facebookClientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      facebookCallbackUrl: process.env.FACEBOOK_CALLBACK_URL,
    },
    azure: {
      azureTenantId: process.env.AZURE_TENANT_ID,
      azureClientId: process.env.AZURE_CLIENT_ID,
      azureClientSecret: process.env.AZURE_CLIENT_SECRET,
      azureCallbackUrl: process.env.AZURE_CALLBACK_URL,
      azureDefaultUrl: process.env.AZURE_DEFAULT_URL,
      azureTargetingVersion: process.env.AZURE_TARGET_VERSION
    }
  };

export type Config = typeof configuration;
export type AppConfig = typeof configuration.app;
export type DbConfig = typeof configuration.db;
export type GoogleConfig = typeof configuration.google
export type FacebookConfig = typeof configuration.facebook
export type AzureConfig = typeof configuration.azure
export type SecretConfig = typeof configuration.secret