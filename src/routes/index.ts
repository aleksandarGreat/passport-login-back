import express from 'express';
import loginRoutes from './login.routes';
import pingRutes from './ping.routes';

const router = express.Router();

router.use('/login', loginRoutes);
router.use('/ping', pingRutes);

export default router;