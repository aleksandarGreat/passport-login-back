import express from 'express';
import { AppConfig, configuration } from '../configs/configuration';
import passport from 'passport';
import getUser, { handleLoginCallback } from '../controller/login.controller';
import requireAuth from '../handlers/login.handler';

const router = express.Router();
const appConfig: AppConfig = configuration.app;

const successLoginUrl = appConfig.successfullLoginUrl;
const failedLoginUrl = appConfig.failedLoginUrl;

router.get('/user', requireAuth, getUser);

router.get('/facebook', passport.authenticate('facebook'));

router.get(
  '/facebook/callback',
  passport.authenticate('facebook', {
    successRedirect: successLoginUrl,
    failureRedirect: failedLoginUrl,
  }),
  (req, res) => handleLoginCallback(req, res, false),
);

router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
router.get(
  '/google/callback',
  passport.authenticate('google', {
    successRedirect: successLoginUrl,
    failureRedirect: failedLoginUrl,
  }),
  (req, res) => handleLoginCallback(req, res, false),
);

router.get('/microsoft', passport.authenticate('curity'));
router.get(
  '/microsoft/callback',
  passport.authenticate('curity', {
    successRedirect: successLoginUrl,
    failureRedirect: failedLoginUrl,
  }),
  (req, res) => handleLoginCallback(req, res, false),
);

export default router;