import { generateToken, decodeToken } from '../../services/token.services';// Update the path as necessary
import jwt from 'jsonwebtoken';
import { User } from '../../models/user.model';

// Mock external dependencies
jest.mock('jsonwebtoken');

// Mock user object
const mockUser: User = {
  _id: 'mockUserId',
  name: 'mockName',
  email: 'mock@example.com',
  provider: 'mockProvider',
  providerId: 'mockProviderId',
  profilePicture: 'mockProfilePicture',
} as any as User;

describe('generateToken', () => {
  it('should generate a token', () => {
    // Mock behavior of jwt.sign
    (jwt.sign as jest.Mock).mockReturnValue('mockToken');

    // Call the function
    const token = generateToken(mockUser);

    // Assertions
    expect(token).toBe('mockToken');
    expect(jwt.sign).toHaveBeenCalledWith(
      expect.objectContaining({
        sub: mockUser.id,
        name: mockUser.name,
        email: mockUser.email,
        provider: mockUser.provider,
        profilePicture: mockUser.profilePicture,
      }),
      expect.any(String), // Check for JWT_SECRET
      expect.objectContaining({
        expiresIn: '30m',
      }),
    );
  });
});

describe('decodeToken', () => {
  it('should decode a valid token', () => {
    // Mock behavior of jwt.verify
    (jwt.verify as jest.Mock).mockReturnValue(mockUser);

    // Call the function
    const decoded = decodeToken('validToken');

    // Assertions
    expect(decoded).toEqual(mockUser);
    expect(jwt.verify).toHaveBeenCalledWith('validToken', expect.any(String));
  });

  it('should handle an invalid token', () => {
    // Mock behavior of jwt.verify to throw an error
    (jwt.verify as jest.Mock).mockImplementation(() => {
      throw new Error('Invalid token');
    });

    // Call the function
    const decoded = decodeToken('invalidToken');

    // Assertions
    expect(decoded).toEqual({ message: 'JWT is not valid, please login again to continue.' });
    expect(jwt.verify).toHaveBeenCalledWith('validToken', expect.any(String));
    expect(jwt.verify).toHaveBeenCalledWith('invalidToken', expect.any(String));
  });
});
