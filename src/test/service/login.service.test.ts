import { Response } from 'express';
import { User } from '../../models/user.model';
import { generateTokenAndSetCookie } from '../../services/login.services';
// import { generateToken } from '../../services/token.services';
import * as tokenServices from '../../services/token.services';


// Mocking the entire login.services module
jest.mock('../../services/login.services');

describe('generateTokenAndSetCookie', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should generate a token and set a cookie with the correct arguments', () => {
      const mockUser: User = {
        _id: 'mockUserId',
        name: 'mockName',
        email: 'mock@example.com',
        provider: 'mockProvider',
        providerId: 'mockProviderId',
        profilePicture: 'mockProfilePicture',
    } as any as User;
      
    const mockResponse = {
      cookie: jest.fn(),
    } as unknown as Response;

    const mockToken = 'mockToken';
    const httpOnly = true;

    // Mocking the behavior of generateToken to return a mock token
    // jest.mock('../../services/token.services', () => ({
    //   generateToken: jest.fn().mockReturnValue(mockToken)
    // }));

    jest.spyOn(tokenServices, 'generateToken').mockReturnValue(mockToken);
    // (generateToken as jest.Mock).mockReturnValue(mockToken);
    // const generateTokenMock = require('../../services/token.services').generateToken as jest.Mock;


    // Calling the function
    generateTokenAndSetCookie(mockUser, mockResponse, httpOnly);

    // Verifying that generateToken was called with the correct arguments
    // expect(tokenServices.generateToken).toHaveBeenCalledWith(mockUser);

    // Verifying that res.cookie was called with the correct arguments
    // expect(mockResponse.cookie).toHaveBeenCalledWith('token', mockToken, expect.objectContaining({ httpOnly }));
  
    // Logging user and token
    console.log('Expected user:', mockUser);
    console.log('Expected token:', mockToken);
  });
});
