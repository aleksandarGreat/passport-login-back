import { NextFunction, Request, Response } from 'express';
import ErrorResponse from '../../exceptions/ErrorResponse';
import { errorHandler, notFound } from '../../exceptions/exceptions';

describe('notFound', () => {
  it('should call next with a Not Found error', () => {
    // Mock objects
    const req = {} as Request;
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    } as unknown as Response;
    const next = jest.fn();

    // Call the notFound function
    notFound(req, res, next);

    // Verify that the response status was set to 404
    expect(res.status).toHaveBeenCalledWith(404);

    // Verify that next was called with an error
    expect(next).toHaveBeenCalledWith(expect.any(Error));
  });
});

describe('errorHandler', () => {
  it('should send error response with correct status code and message', () => {
    // Mock objects
    const err = new Error('Test error message');
    const req = {} as Request;
    const res = {
      statusCode: 500,
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    } as unknown as Response<ErrorResponse>;
    const next = jest.fn();

    // Call the errorHandler function
    errorHandler(err, req, res, next);

    // Verify that the response status was set to the correct status code
    expect(res.status).toHaveBeenCalledWith(500);

    // Verify that the response JSON contains the error message
    expect(res.json).toHaveBeenCalledWith({ message: 'Test error message' });
  });

  it('should handle response status code if not equal to 200', () => {
    // Mock objects
    const err = new Error('Test error message');
    const req = {} as Request;
    const res = {
      statusCode: 404,
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    } as unknown as Response<ErrorResponse>;
    const next = jest.fn();

    // Call the errorHandler function
    errorHandler(err, req, res, next);

    // Verify that the response status was set to the correct status code
    expect(res.status).toHaveBeenCalledWith(404);

    // Verify that the response JSON contains the error message
    expect(res.json).toHaveBeenCalledWith({ message: 'Test error message' });
  });
});
