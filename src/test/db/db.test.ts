import { connect } from 'mongoose';
import connectDB from '../../db/db';
import { configuration, DbConfig } from '../../configs/configuration';

jest.mock('mongoose');

const dbConfig: DbConfig = configuration.db;

describe('connectDB', () => {
  it('should connect to the MongoDB database successfully', async () => {
    // Mock the mongoose connect function to resolve successfully
    (connect as jest.Mock).mockResolvedValueOnce(undefined);

    // Suppress console logs during the test
    const consoleSpy = jest.spyOn(console, 'log').mockImplementation();

    // Call the connectDB function
    await connectDB();

    // Verify that the mongoose connect function was called with the correct URI
    expect(connect).toHaveBeenCalledWith(`${dbConfig.uri}`);

    // Verify that the success message was logged
    expect(consoleSpy).toHaveBeenCalledWith('MongoDB connected successfully on the given URL.');
    
    // Restore the console log function
    consoleSpy.mockRestore();
  });

  it('should exit the process with failure if connection fails', async () => {
    // Mock the mongoose connect function to reject with an error
    const mockError = new Error('Connection error');
    (connect as jest.Mock).mockRejectedValueOnce(mockError);

    // Mock process.exit to avoid exiting during the test
    const exitSpy = jest.spyOn(process, 'exit').mockReturnValue(undefined as never);

    // Suppress console errors during the test
    const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation();

    // Call the connectDB function
    await connectDB();

    // Verify that the mongoose connect function was called with the correct URI
    expect(connect).toHaveBeenCalledWith(`${dbConfig.uri}`);

    // Verify that the error message was logged
    expect(consoleErrorSpy).toHaveBeenCalledWith('Connection error');

    // Verify that the process exited with failure
    expect(exitSpy).toHaveBeenCalledWith(1);

    // Restore the console error and process exit functions
    consoleErrorSpy.mockRestore();
    exitSpy.mockRestore();
  });
});
