import * as dotenv from 'dotenv';
import { configuration, Config, AppConfig, DbConfig, GoogleConfig, FacebookConfig, AzureConfig, SecretConfig } from '../../configs/configuration';

// Mock dotenv config
jest.mock('dotenv', () => ({
  config: jest.fn().mockReturnValue({ parsed: process.env }),
}));

describe('Configuration', () => {
  let originalProcessEnv: NodeJS.ProcessEnv;

  beforeAll(() => {
    // Store original environment variables
    originalProcessEnv = process.env;

    // Create a mock environment object matching the .env file
    const mockEnv = {
      PORT: '3000',
      SUCCESSFULL_LOGIN_URL: 'your_successfull_login_url',
      FAILED_LOGIN_URL: 'your_failed_login_url',
      CORS_ORIGIN: '*',
      DB_URL: 'mongodb://localhost:27017',
      DB_USERNAME: '',
      DB_PASS: '',
      SESSION: 'keyboard cat',
      JWT_SECRET: 'your_jwt_secret',
      GOOGLE_CLIENT_ID: 'your_google_client_id',
      GOOGLE_CLIENT_SECRET: 'your_google_client_secret',
      GOOGLE_CALLBACK_URL: 'your_google_callback_url',
      FACEBOOK_CLIENT_ID: 'your_facebook_client_id',
      FACEBOOK_CLIENT_SECRET: 'your_facebook_client_secret',
      FACEBOOK_CALLBACK_URL: 'your_facebook_callback_url',
      AZURE_TENANT_ID: 'your_azure_tenant_id',
      AZURE_CLIENT_ID: 'your_azure_client_id',
      AZURE_CLIENT_SECRET: 'your_azure_client_secret',
      AZURE_CALLBACK_URL: 'your_azure_callback_url',
    };

    // Assign the mock environment to process.env
    process.env = mockEnv;

    // Mock the dotenv config function to resolve immediately
    (dotenv.config as jest.Mock).mockImplementation(() => {
        Object.assign(process.env, mockEnv);
      });
  });

  afterAll(() => {
    // Restore original environment variables
    process.env = originalProcessEnv;
  });

  it('should load configuration from environment variables', () => {
    // Call the configuration file
    const config: Config = configuration;

    // Verify AppConfig
    const appConfig: AppConfig = config.app;
    expect(appConfig.port).toBe('3000');
    expect(appConfig.corsOrigin).toBe('*');
    expect(appConfig.successfullLoginUrl).toBe('your_successfull_login_url');
    expect(appConfig.failedLoginUrl).toBe('your_failed_login_url');

    // Verify SecretConfig
    const secretConfig: SecretConfig = config.secret;
    expect(secretConfig.jwtSecret).toBe('your_jwt_secret');
    expect(secretConfig.sessionSecret).toBe('keyboard cat');

    // Verify DbConfig
    const dbConfig: DbConfig = config.db;
    expect(dbConfig.uri).toBe('mongodb://localhost:27017');

    // Verify GoogleConfig
    const googleConfig: GoogleConfig = config.google;
    expect(googleConfig.googleClientId).toBe('your_google_client_id');
    expect(googleConfig.googleClientSecret).toBe('your_google_client_secret');
    expect(googleConfig.googleCallbackUrl).toBe('your_google_callback_url');

    // Verify FacebookConfig
    const facebookConfig: FacebookConfig = config.facebook;
    expect(facebookConfig.facebookClientId).toBe('your_facebook_client_id');
    expect(facebookConfig.facebookClientSecret).toBe('your_facebook_client_secret');
    expect(facebookConfig.facebookCallbackUrl).toBe('your_facebook_callback_url');

    // Verify AzureConfig
    const azureConfig: AzureConfig = config.azure;
    expect(azureConfig.azureTenantId).toBe('your_azure_tenant_id');
    expect(azureConfig.azureClientId).toBe('your_azure_client_id');
    expect(azureConfig.azureClientSecret).toBe('your_azure_client_secret');
    expect(azureConfig.azureCallbackUrl).toBe('your_azure_callback_url');
    expect(azureConfig.azureDefaultUrl).toBe('https://login.microsoftonline.com');
    expect(azureConfig.azureTargetingVersion).toBe('v2.0');
  });
});
