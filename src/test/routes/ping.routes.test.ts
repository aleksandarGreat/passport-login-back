import request from 'supertest';
import express from 'express';
import pingRoutes from '../../routes/ping.routes';

describe('Ping Routes', () => {
  const app = express();
  app.use('/ping', pingRoutes);

  it('should respond with status 200 and "Health check"', async () => {
    const response = await request(app).get('/ping');
    expect(response.status).toBe(200);
    expect(response.text).toBe('Health check');
  });
});
