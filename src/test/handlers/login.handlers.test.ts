import { Request, Response, NextFunction } from 'express';
import requireAuth from '../../handlers/login.handler';

describe('requireAuth middleware', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;
  let next: jest.Mock<NextFunction>;

  beforeEach(() => {
    req = {} as Partial<Request>;
    res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    } as Partial<Response>;
    next = jest.fn();
  });

  it('should call next if user is authenticated', () => {
    req.user = {
        id: 'mockUserId',
        name: 'mockName',
        email: 'mock@example.com',
        provider: 'mockProvider',
        providerId: 'mockProviderId',
        profilePicture: 'mockProfilePicture',
    };

    requireAuth(req as Request, res as Response, next);

    expect(next).toHaveBeenCalled();
    expect(res.status).not.toHaveBeenCalled();
    expect(res.json).not.toHaveBeenCalled();
  });

  it('should return 401 error if user is not authenticated', () => {
    requireAuth(req as Request, res as Response, next);

    expect(next).not.toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({
      error: true,
      message: 'The user is not authenticated, aborting the request.',
    });
  });
});