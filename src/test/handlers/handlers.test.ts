import { Request, Response } from "express";
import { notFound } from "../../handlers/handler";

// Mock Request, Response, and NextFunction objects
const mockRequest = {
    originalUrl: '/test-url' // Adding originalUrl property to mock request
  } as Request;
  const mockResponse = {
    status: jest.fn(function(code) {
      return this;
    }),
    send: jest.fn(),
  } as unknown as Response;
const mockNext = jest.fn();

describe('notFound function', () => {
  it('should set status to 404 and call next with an error', () => {
    // Call the function with mock objects
    notFound(mockRequest, mockResponse, mockNext);

    // Check if status is set to 404
    expect(mockResponse.status).toHaveBeenCalledWith(404);

    // Check if next is called with an error containing the original URL
    expect(mockNext).toHaveBeenCalledWith(expect.any(Error));
    expect(mockNext.mock.calls[0][0].message).toContain('🔍 - Not Found -');
  });
});