import { Request, Response } from 'express';
import getUser from '../../controller/user.controller';
import { generateToken } from '../../services/token.services'; // Assuming this is the path to your generateToken function

jest.mock('../../services/token.services');

describe('getUser function', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;

  beforeEach(() => {
    req = {
      user: {
        _id: 'mockUserId',
        name: 'mockName',
        email: 'mock@example.com',
        provider: 'mockProvider',
        providerId: 'mockProviderId',
        profilePicture: 'mockProfilePicture',
    } 
    } as Partial<Request>;
    res = {
      cookie: jest.fn(),
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    } as Partial<Response>;
  });

  it('should set token cookie and return user details with token if user is authenticated', async () => {
    const mockToken = 'mockToken';
    (generateToken as jest.Mock).mockReturnValue(mockToken);

    await getUser(req as Request, res as Response);

    expect(res.cookie).toHaveBeenCalledWith('token', mockToken, { httpOnly: true });
    expect(res.json).toHaveBeenCalledWith({
      error: false,
      message: 'User has been authenticated successfully.',
      token: mockToken,
    });
    expect(res.status).not.toHaveBeenCalled();
  });

  it('should return 500 status and error message if an error occurs', async () => {
    const errorMessage = 'Internal Server Error';
    (generateToken as jest.Mock).mockImplementation(() => {
      throw new Error(errorMessage);
    });

    await getUser(req as Request, res as Response);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      message: errorMessage,
    });
    expect(res.cookie).not.toHaveBeenCalled();
  });
});
