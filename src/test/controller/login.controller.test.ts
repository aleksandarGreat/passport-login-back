import { Request, Response } from 'express';
import getUser from '../../controller/user.controller';

describe('getUser function', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;

  beforeEach(() => {
    req = {
      user: { /* Mock user object */ },
    } as Partial<Request>;
    res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    } as Partial<Response>;
  });

  it('should return 500 status and error message if an error occurs', async () => {
    const errorMessage = 'Internal Server Error';
    req.user = undefined; // Simulate req.user not being defined

    await getUser(req as Request, res as Response);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Cannot read properties of undefined (reading \'id\')',
    });
  });
});
