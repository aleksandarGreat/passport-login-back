import mongoose, { Document } from 'mongoose';

const Schema = mongoose.Schema;

export type User = Document & {
    email: string;
    name: string;
    providerId: string;
    provider: string;
    profilePicture?: string;
  };

const userSchema = new Schema<User>({
    email: { type: String, required: true },
    name: { type: String, required: true },
    provider: { type: String, required: true },
    providerId: { type: String, required: true },
    profilePicture: { type: String, required: false },
  }, { timestamps: true });

const User = mongoose.model<User>('User', userSchema);

export default User;