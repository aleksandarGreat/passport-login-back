import jwt from 'jsonwebtoken';
import { SecretConfig, configuration } from '../configs/configuration';
import ErrorResponse from '../exceptions/ErrorResponse';
import { User } from '../models/user.model';

const secretConfig: SecretConfig = configuration.secret;

const decodeToken = (token: string): User | ErrorResponse => {
    try {
      const decoded = jwt.verify(token, secretConfig.jwtSecret!) as User;
      return decoded;
    } catch (error) {
      return { message: 'JWT is not valid, please login again to continue.' };
    }
  };

const generateToken = (user: User): string => {
  const payload = {
    sub: user.id,
    name: user.name,
    email: user.email,
    provider: user.provider,
    profilePicture: user?.profilePicture,
  };

  return jwt.sign(payload, secretConfig.jwtSecret!, {
    expiresIn: '30m',
  });
};

export { generateToken, decodeToken };