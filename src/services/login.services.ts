import { Response } from 'express';
import { generateToken } from './token.services';
import { User } from '../models/user.model';

export function generateTokenAndSetCookie(user: User, res: Response, httpOnly: boolean) {
  console.log(user);
  const token = generateToken(user);
  console.log(token)
  res.cookie('token', token, { httpOnly });
}
