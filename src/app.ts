require('dotenv').config();
import express from 'express';
import passport from 'passport';
import { AppConfig, configuration, SecretConfig } from './configs/configuration';
import routes from './routes';
import * as handlers from './handlers/handler';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import session from 'express-session';

const app = express();
const appConfig: AppConfig = configuration.app;
const secretConfig: SecretConfig = configuration.secret;

app.use(session({
    secret: secretConfig.sessionSecret!,
    resave: false,
    saveUninitialized: true,
  }));
  
  app.use(passport.initialize());
  app.use(passport.session());
  
  app.use(cookieParser());
  app.use(morgan('dev'));
  app.use(helmet());
  app.use(bodyParser.json());
  
  app.use(cors({
    // For not it can be '*'
    // different ones can be set depending on the final implementation
    origin: appConfig.corsOrigin,
    methods: ['GET', 'POST', 'PUT', 'HEAD', 'PATCH', 'DELETE'],
    credentials: true,
  }));

  var csrf = require('csurf')
  app.use(csrf({ cookie: true }));
  
  app.use(express.json());
  
  app.use(routes);
  
  app.use(handlers.notFound);
  app.use(handlers.errorHandler);

export default app;