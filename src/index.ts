import dotenv from 'dotenv';
import app from './app';
import { AppConfig, configuration } from './configs/configuration';
import connectDB from './db/db';

dotenv.config();

const appConfig: AppConfig = configuration.app;

const port = appConfig.port || 3000;

connectDB().then(() => {
  app.listen(port, () => {
    console.log(`Listening: http://localhost:${port}`);
  });
});