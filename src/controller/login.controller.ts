import { Request, Response } from 'express';
import { User } from '../models/user.model';

export function handleLoginCallback(req: Request, res: Response, httpOnly: boolean) {
}

const getUser = async (req: Request, res: Response) => {
    try {
      const user = req.user as User;
    } catch (error) {
      res.status(500).json({
        message: (error as Error).message,
      });
    }
  };
  
  export default getUser;