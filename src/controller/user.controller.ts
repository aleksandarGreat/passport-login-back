import { Request, Response } from 'express';
import { User } from '../models/user.model';
import { generateToken } from '../services/token.services';

const getUser = async (req: Request, res: Response) => {
  try {
    const user = req.user as User;
    const token = generateToken(user);
    res.cookie('token', token, { httpOnly: true });
    res.json({
      error: false,
      message: 'User has been authenticated successfully.',
      token: token,
    });

  } catch (error) {
    res.status(500).json({
      message: (error as Error).message,
    });
  }
};

export default getUser;