"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.configuration = void 0;
const dotenv = __importStar(require("dotenv"));
const path_1 = require("path");
const process = __importStar(require("process"));
dotenv.config({ path: (0, path_1.join)(process.cwd(), '.env') });
exports.configuration = {
    app: {
        port: process.env.PORT,
        corsOrigin: process.env.CORS_ORIGIN,
        successfullLoginUrl: process.env.SUCCESSFULL_LOGIN_URL,
        failedLoginUrl: process.env.FAILED_LOGIN_URL,
    },
    secret: {
        jwtSecret: process.env.JWT_SECRET,
        sessionSecret: process.env.SESSION,
    },
    db: {
        uri: process.env.DB_URL,
    },
    google: {
        googleClientId: process.env.GOOGLE_CLIENT_ID,
        googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
        googleCallbackUrl: process.env.GOOGLE_CALLBACK_URL,
    },
    facebook: {
        facebookClientId: process.env.FACEBOOK_CLIENT_ID,
        facebookClientSecret: process.env.FACEBOOK_CLIENT_SECRET,
        facebookCallbackUrl: process.env.FACEBOOK_CALLBACK_URL,
    },
    azure: {
        azureTenantId: process.env.AZURE_TENANT_ID,
        azureClientId: process.env.AZURE_CLIENT_ID,
        azureClientSecret: process.env.AZURE_CLIENT_SECRET,
        azureCallbackUrl: process.env.AZURE_CALLBACK_URL,
    }
};
//# sourceMappingURL=configuration.js.map