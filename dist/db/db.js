"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const configuration_1 = require("../configs/configuration");
const dbConfig = configuration_1.configuration.db;
const connectDB = async () => {
    try {
        await (0, mongoose_1.connect)(dbConfig.uri);
        console.log('MongoDB connected successfully on the given URL.');
    }
    catch (err) {
        console.error(err.message);
        // Exit process with failure
        process.exit(1);
    }
};
exports.default = connectDB;
//# sourceMappingURL=db.js.map