"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const configuration_1 = require("../configs/configuration");
const passport_1 = __importDefault(require("passport"));
const login_controller_1 = __importStar(require("../controller/login.controller"));
const login_handler_1 = __importDefault(require("../handlers/login.handler"));
const router = express_1.default.Router();
const appConfig = configuration_1.configuration.app;
const successLoginUrl = appConfig.successfullLoginUrl;
const failedLoginUrl = appConfig.failedLoginUrl;
router.get('/user', login_handler_1.default, login_controller_1.default);
router.get('/facebook', passport_1.default.authenticate('facebook'));
router.get('/facebook/callback', passport_1.default.authenticate('facebook', {
    successRedirect: successLoginUrl,
    failureRedirect: failedLoginUrl,
}), (req, res) => (0, login_controller_1.handleLoginCallback)(req, res, false));
router.get('/google', passport_1.default.authenticate('google', { scope: ['profile', 'email'] }));
router.get('/google/callback', passport_1.default.authenticate('google', {
    successRedirect: successLoginUrl,
    failureRedirect: failedLoginUrl,
}), (req, res) => (0, login_controller_1.handleLoginCallback)(req, res, false));
router.get('/microsoft', passport_1.default.authenticate('curity'));
router.get('/microsoft/callback', passport_1.default.authenticate('curity', {
    successRedirect: successLoginUrl,
    failureRedirect: failedLoginUrl,
}), (req, res) => (0, login_controller_1.handleLoginCallback)(req, res, false));
exports.default = router;
//# sourceMappingURL=login.routes.js.map