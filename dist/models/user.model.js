"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const userSchema = new Schema({
    email: { type: String, required: true },
    name: { type: String, required: true },
    provider: { type: String, required: true },
    providerId: { type: String, required: true },
    profilePicture: { type: String, required: false },
}, { timestamps: true });
const User = mongoose_1.default.model('User', userSchema);
//# sourceMappingURL=user.model.js.map