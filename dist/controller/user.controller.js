"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const token_services_1 = require("../services/token.services");
const getUser = async (req, res) => {
    try {
        const user = req.user;
        const token = (0, token_services_1.generateToken)(user);
        res.cookie('token', token, { httpOnly: true });
        res.json({
            error: false,
            message: 'User has been authenticated successfully.',
            token: token,
        });
    }
    catch (error) {
        res.status(500).json({
            message: error.message,
        });
    }
};
exports.default = getUser;
//# sourceMappingURL=user.controller.js.map