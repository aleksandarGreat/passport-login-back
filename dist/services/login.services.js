"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateTokenAndSetCookie = void 0;
const token_services_1 = require("./token.services");
function generateTokenAndSetCookie(user, res, httpOnly) {
    console.log(user);
    const token = (0, token_services_1.generateToken)(user);
    console.log(token);
    res.cookie('token', token, { httpOnly });
}
exports.generateTokenAndSetCookie = generateTokenAndSetCookie;
//# sourceMappingURL=login.services.js.map