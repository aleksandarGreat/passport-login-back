"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decodeToken = exports.generateToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const decodeToken = (token) => {
    try {
        const decoded = jsonwebtoken_1.default.verify(token, process.env.JWT_SECRET);
        return decoded;
    }
    catch (error) {
        return { message: 'JWT is not valid, please login again to continue.' };
    }
};
exports.decodeToken = decodeToken;
const generateToken = (user) => {
    const payload = {
        sub: user.id,
        name: user.name,
        email: user.email,
        provider: user.provider,
        profilePicture: user?.profilePicture,
    };
    return jsonwebtoken_1.default.sign(payload, process.env.JWT_SECRET, {
        expiresIn: '30m',
    });
};
exports.generateToken = generateToken;
//# sourceMappingURL=token.services.js.map