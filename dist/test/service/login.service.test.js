"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const login_services_1 = require("../../services/login.services");
// Mocking the entire login.services module
jest.mock('../../services/login.services');
describe('generateTokenAndSetCookie', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('should generate a token and set a cookie with the correct arguments', () => {
        const mockUser = {
            id: 'mockUserId',
            name: 'mockName',
            email: 'mock@example.com',
            provider: 'mockProvider',
            providerId: 'mockProviderId',
            profilePicture: 'mockProfilePicture',
        };
        const mockResponse = {
            cookie: jest.fn(),
        };
        const mockToken = 'mockToken';
        // Mocking the behavior of generateToken to return a mock token
        jest.mock('../../services/token.services', () => ({
            generateToken: jest.fn().mockReturnValue(mockToken)
        }));
        const generateTokenMock = require('../../services/token.services').generateToken;
        // Calling the function
        (0, login_services_1.generateTokenAndSetCookie)(mockUser, mockResponse, true);
        // Verifying that generateToken was called with the correct arguments
        expect(generateTokenMock).toHaveBeenCalledWith(mockUser);
        // Verifying that res.cookie was called with the correct arguments
        expect(mockResponse.cookie).toHaveBeenCalledWith('token', mockToken, { httpOnly: true });
    });
});
//# sourceMappingURL=login.service.test.js.map