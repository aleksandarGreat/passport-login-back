"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const token_services_1 = require("../../services/token.services"); // Update the path as necessary
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
// Mock external dependencies
jest.mock('jsonwebtoken');
// Mock user object
const mockUser = {
    id: 'mockUserId',
    name: 'mockName',
    email: 'mock@example.com',
    provider: 'mockProvider',
    providerId: 'mockProviderId',
    profilePicture: 'mockProfilePicture',
};
describe('generateToken', () => {
    it('should generate a token', () => {
        // Mock behavior of jwt.sign
        jsonwebtoken_1.default.sign.mockReturnValue('mockToken');
        // Call the function
        const token = (0, token_services_1.generateToken)(mockUser);
        // Assertions
        expect(token).toBe('mockToken');
        expect(jsonwebtoken_1.default.sign).toHaveBeenCalledWith(expect.objectContaining({
            sub: mockUser.id,
            name: mockUser.name,
            email: mockUser.email,
            provider: mockUser.provider,
            profilePicture: mockUser.profilePicture,
        }), expect.any(String), // Check for JWT_SECRET
        expect.objectContaining({
            expiresIn: '15m',
        }));
    });
});
describe('decodeToken', () => {
    it('should decode a valid token', () => {
        // Mock behavior of jwt.verify
        jsonwebtoken_1.default.verify.mockReturnValue(mockUser);
        // Call the function
        const decoded = (0, token_services_1.decodeToken)('validToken');
        // Assertions
        expect(decoded).toEqual(mockUser);
        expect(jsonwebtoken_1.default.verify).toHaveBeenCalledWith('validToken', expect.any(String));
    });
    it('should handle an invalid token', () => {
        // Mock behavior of jwt.verify to throw an error
        jsonwebtoken_1.default.verify.mockImplementation(() => {
            throw new Error('Invalid token');
        });
        // Call the function
        const decoded = (0, token_services_1.decodeToken)('invalidToken');
        // Assertions
        expect(decoded).toEqual({ message: 'Invalid JWT' });
        expect(jsonwebtoken_1.default.verify).toHaveBeenCalledWith('validToken', expect.any(String), 'invalidToken', expect.any(String));
    });
});
//# sourceMappingURL=token.service.test.js.map