"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const app_1 = __importDefault(require("./app"));
const configuration_1 = require("./configs/configuration");
const db_1 = __importDefault(require("./db/db"));
dotenv_1.default.config();
const appConfig = configuration_1.configuration.app;
const port = appConfig.port || 3000;
(0, db_1.default)().then(() => {
    app_1.default.listen(port, () => {
        console.log(`Listening: http://localhost:${port}`);
    });
});
//# sourceMappingURL=index.js.map