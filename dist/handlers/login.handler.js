"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const requireAuth = (req, res, next) => {
    if (req.user) {
        next();
    }
    else {
        res.status(401).json({
            error: true,
            message: 'The user is not authenticated, aborting the request.',
        });
    }
};
exports.default = requireAuth;
//# sourceMappingURL=login.handler.js.map